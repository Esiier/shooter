﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float health = 50f, speed = 4f;
    public bool chase;

    private Transform Player;

    public void TakeDamage (float amount)
    {
        health -= amount;
        if(health <=0f)
        {
            Die();
            PointCounter.points++;
        }
    }

    void Start() 
    {
        Player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
    }

    void Update() 
    {
        if(chase)
            transform.position = Vector3.MoveTowards(transform.position, Player.position, speed * Time.deltaTime);   
            
    }
    
    void Die()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Player")
            chase = true;
    }
}
