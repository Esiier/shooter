﻿using UnityEngine;

public class Gun : MonoBehaviour
{
  
    public float damage = 10f;
    public float range = 100f;

    public GameObject arma1, arma2;
    public static bool Active = false;
    public int Debugg=0;

    public Camera fpsCam;

    void Start()
    {
       //arma1.SetActive(true);
    } 
   void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }

        if(Active)
        {
            arma1.SetActive(true);
            arma2.SetActive(false);
            Debug.Log("Arama1 is active" + Active);
        }
        if(!Active)
        {
            arma1.SetActive(false);
            arma2.SetActive(true);
            Debug.Log("Arama1 inactive" + Active);
        }

        
            
            
    
    }

    void Shoot () 
    {
        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range))
        {
            //Debug.Log(hit.transform.name);

            Enemy target = hit.transform.GetComponent<Enemy>();

            if(target !=null)
            {
                target.TakeDamage(damage);
            }
        }
    }

    void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "1")
            Active = true;
            Debugg=1;
        if(other.gameObject.tag == "2")
            Active = false;
            Debugg=2;
    }

}
