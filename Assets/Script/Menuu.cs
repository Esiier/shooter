﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Menuu : MonoBehaviour
{

    public GameObject panel;
    private bool panelACtivate;
    private float speed = 10000;
    private float varPanelX;

    
    void Start(){
        //SoundManager.Instance.playMenuMusic();
        panelACtivate = false;
        varPanelX = panel.transform.position.x;
    }

    void Update(){

        if(panelACtivate && panel.transform.position.x > transform.position.x+50){
            panel.transform.Translate(Vector3.left*Time.deltaTime*speed);
        }

        if(!panelACtivate && panel.transform.position.x < varPanelX){
            panel.transform.Translate(Vector3.right*Time.deltaTime*speed);
        }

    }

    public void Play(){
        //SoundManager.Instance.playTargetSound();
        SceneManager.LoadScene("main");

    }
    

    public void HightScore(){
        //SoundManager.Instance.playTargetSound();
        panelACtivate = true;

    }
    public void Back(){
        //SoundManager.Instance.playTargetSound();
        panelACtivate = false;

    }

    public void Exit(){
        //SoundManager.Instance.playTargetSound();
        Application.Quit();
    }
}
