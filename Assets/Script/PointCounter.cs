﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class PointCounter : MonoBehaviour
{

    public static float points = 0;
    TextMeshProUGUI scoreText;
    
        void Start()
    {
        scoreText = GetComponent<TextMeshProUGUI> ();
    }
    void Update()
    {
        scoreText.text = points.ToString() + "00";
    }

}
