﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public class Spawn
    {
        public string name;
        public Transform enemy;
        public int count;
        public float rate;
    }

    public Spawn[] spawns;
    private int nextSpawn = 0;
    public float timeTillNextSpawn = 5f;
}
